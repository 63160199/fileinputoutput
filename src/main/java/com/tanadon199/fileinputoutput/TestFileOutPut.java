/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.fileinputoutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Kitty
 */
public class TestFileOutPut {
    public static void main(String[]arrgs){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            Dog dog = new Dog("Kitty",5);
            Rectangle rec = new Rectangle(5,4);
            File file = new File("dog.obj");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(dog);
            oos.writeObject(rec);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            System.out.println("File not Found");
        } catch (IOException ex) {
            Logger.getLogger(TestFileOutPut.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(TestFileOutPut.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
