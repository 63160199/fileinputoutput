/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.fileinputoutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kitty
 */
public class Computer implements Serializable{

    int i = 0;

    private int hand, playerHand;
    private int win;
    private int draw;
    private int lose;

    public Computer() {
    }
    
    

    private int randomm() {
        return 0 + (int) (Math.random() * 3);
    }

    public int paoYing(int playerHand) {
        this.playerHand = playerHand;
        this.hand = randomm();
        if (this.playerHand == this.hand) {
            draw++;
            return 0;
        } else if (this.playerHand == 0 && this.hand == 2) {
            win++;
            return 1;
        } else if (this.playerHand == 1 && this.hand == 0) {
            win++;
            return 1;
        } else if (this.playerHand == 2 && this.hand == 1) {
            win++;
            return 1;
        } else {
            lose++;
            return -1;
        }

    }

    public int getHand() {
        return hand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getDraw() {
        return draw;
    }

    public int getLose() {
        return lose;
    }
}
